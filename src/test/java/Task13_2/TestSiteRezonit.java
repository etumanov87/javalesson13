package Task13_2;


import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import java.time.Duration;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;


public class TestSiteRezonit {

    @Test
    public void testPositiveScenarioWebsite() {
        pageLoadTimeout = 50000;
        open("https://www.rezonit.ru/");

        SelenideElement itemUrgentPrintedCircuitBoards = $x("//ul[contains(@class,'main-menu')]//a[contains(text(),'Срочные печатные')]");
        itemUrgentPrintedCircuitBoards.click();

        SelenideElement fieldLength = $x("//input[@placeholder='длина']");
        fieldLength.setValue("10");

        SelenideElement fieldWidth = $x("//input[@placeholder='ширина']");
        fieldWidth.setValue("20");

        SelenideElement fieldNumberOfPieces = $x("//input[@name='count']");
        fieldNumberOfPieces.setValue("20");

        SelenideElement buttonCalculate = $x("//button[@id='calculate']");
        buttonCalculate.click();

        SelenideElement checkVisibleTextWithPrice = $x("//span[@id='total-price']");
        checkVisibleTextWithPrice.shouldBe(visible, Duration.ofSeconds(20));
    }


    @Test
    public void testNegativeScenarioWebsite() {
        pageLoadTimeout = 50000;
        open("https://www.rezonit.ru/");

        SelenideElement itemUrgentPrintedCircuitBoards = $x("//ul[contains(@class,'main-menu')]//a[contains(text(),'Срочные печатные')]");
        itemUrgentPrintedCircuitBoards.click();

        SelenideElement fieldLength = $x("//input[@placeholder='длина']");
        fieldLength.setValue("10");

        SelenideElement fieldWidth = $x("//input[@placeholder='ширина']");
        fieldWidth.setValue("-999");

        SelenideElement checkVisibleBlockWithRedWarningIcon = $x("//div[@class='alert alert-danger']");
        checkVisibleBlockWithRedWarningIcon.shouldBe(visible);

        SelenideElement checkVisibleTextIncorrectData = $x("//div[text()='Ширина болжна быть от 5 до 475 мм']");
        checkVisibleTextIncorrectData.shouldBe(visible);
    }

}
