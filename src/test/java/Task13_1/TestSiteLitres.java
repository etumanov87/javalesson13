package Task13_1;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class TestSiteLitres {

    @Test
    public void testCheckSiteLitres() {
        open("https://www.litres.ru/");
        SelenideElement searchBar = $x("//input[@data-test-id='header__search-input--desktop']");
        searchBar.setValue("грокаем алгоритмы");

        SelenideElement buttonSearchBar = $x("//button[@data-test-id='header__search-button--desktop']");
        buttonSearchBar.click();

        SelenideElement firstProductSearchResults = $x("//div[@data-test-id='art__cover--desktop']");
        firstProductSearchResults.click();

        SelenideElement priceSubscription = $x("//div[@data-test-id='book-sale-block__abonement--wrapper']");
        priceSubscription.shouldBe(visible);

        SelenideElement priceForDownload = $x("//div[@data-test-id='book-sale-block__PPD--wrapper']");
        priceForDownload.shouldBe(visible);

        SelenideElement buttonAddToCart = $x("//button[@data-test-id='book__addToCartButton--desktop']");
        buttonAddToCart.shouldBe(visible);
    }
}


